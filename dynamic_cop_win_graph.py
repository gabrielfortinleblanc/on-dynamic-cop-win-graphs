"""
This module contains the algorithm presented in the article "On dynamic cop-win
graphs".
"""
from math import inf
import sys
import networkx as nx
import logging


def cop_win(graph, verbose=False):
    """
    Compute if the graph `graph` is cop-win.

    Parameters
    ----------
    graph : Networkx Graph
        A Networkx Graph to analyze if it is cop-win or not. Each edges of this
        graph has to have an attribute named `t` which is a 0-1 string where 0
        at the index i of the string means the edge does not exist at the time
        step i and where 1 at the same index means the edge does exist at the
        time step i.
    verbose : boolean
        If True is specified, then verbose mode is activated. It prints the
        alpha variable for the edge (u, v) and the timestep `t` the change has
        been done.

    Returns
    -------
    A boolean is returned meaning if the graph `graph` is cop-win or not.
    """
    logger = logging.getLogger('cop_win')
    if verbose:
        logger.setLevel(logging.INFO)
        logger_handler = logging.StreamHandler(stream=sys.stdout)
        logger_handler.setLevel(logging.INFO)
        logger.addHandler(logger_handler)

    if graph.number_of_edges() == 0:
        return True if graph.number_of_nodes() < 2 else False
    time_length = len(graph[0][1]['t'])

    cost = dict()
    for robber_pos in graph.nodes():
        for cop_pos in graph.nodes():
            for t in range(time_length):
                # False = Robber and True = Cop
                if robber_pos != cop_pos:
                    cost[(robber_pos, cop_pos, t, False)] = inf
                    cost[(robber_pos, cop_pos, t, True)] = inf
                else:
                    cost[(robber_pos, cop_pos, t, False)] = 0
                    cost[(robber_pos, cop_pos, t, True)] = 0

    changes = True
    while changes:
        changes = False
        for robber_pos in graph.nodes():
            for cop_pos in graph.nodes():
                if robber_pos == cop_pos:
                    continue
                for t in range(time_length):
                    if cost[(robber_pos, cop_pos, t, True)] == inf:
                        # Evaluate cop possible moves
                        neighbors_values = [
                            cost[(robber_pos, n, t, False)]
                            for n in graph.neighbors(cop_pos)
                            if graph[cop_pos][n]['t'][t] == '1'
                        ]
                        # The cop can stay on the same vertex.
                        neighbors_values.append(
                            cost[(robber_pos, cop_pos, t, False)]
                        )
                        new_value = 1 + min(neighbors_values)
                        if new_value != cost[(robber_pos, cop_pos, t, True)]:
                            logger.info(f'Robber position: {robber_pos:4d}\t'
                                        f'Cop position: {cop_pos:4d}\t'
                                        f'alpha: {new_value:4d}\t'
                                        f'timestep: {t:4d}')
                            cost[(robber_pos, cop_pos, t, True)] = new_value
                            changes = True

                    if cost[(robber_pos, cop_pos, t, False)] == inf:
                        # Evaluate robber possible moves
                        neighbors_values = [
                            cost[(n, cop_pos, (t + 1) % time_length, True)]
                            for n in graph.neighbors(robber_pos)
                            if graph [robber_pos][n]['t'][t] == '1'
                        ]
                        # The robber can stay on the same vertex.
                        neighbors_values.append(
                            cost[(robber_pos, cop_pos, (t + 1) % time_length,
                                 True)]
                        )
                        new_value = max(neighbors_values)
                        if new_value != cost[(robber_pos, cop_pos, t, False)]:
                            logger.info(f'Robber position: {robber_pos:4d}\t'
                                        f'Cop position: {cop_pos:4d}\t'
                                        f'alpha: {new_value:4d}\t'
                                        f'timestep: {t:4d}')
                            cost[(robber_pos, cop_pos, t, False)] = new_value
                            changes = True

    for robber_pos in graph.nodes():
        winning_pos = 0
        for cop_pos in graph.nodes():
            if cost[(cop_pos, robber_pos, 0, True)] < inf:
                winning_pos += 1
        if winning_pos == graph.number_of_nodes():
            return True
    return False


if __name__ == '__main__':
    # A complete graph of 4 vertices
    complete_graph = nx.complete_graph(4)
    for robber_pos, cop_pos in complete_graph.edges():
        complete_graph [robber_pos][cop_pos]['t'] = '1'
    # assert cop_win(complete_graph)

    # A cycle graph of length 4
    cycle_graph = nx.complete_graph(4)
    for robber_pos, cop_pos in cycle_graph.edges():
        if (robber_pos + 1) % 4 == cop_pos or (robber_pos - 1) % 4 == cop_pos:
            cycle_graph [robber_pos][cop_pos]['t'] = '1'
        else:
            cycle_graph [robber_pos][cop_pos]['t'] = '0'
    # assert not cop_win(cycle_graph)

    # A star of length 1
    star_graph = nx.complete_graph(5)
    for robber_pos, cop_pos in star_graph.edges():
        star_graph [robber_pos][cop_pos]['t'] = '0'
    for i in range(1, 5):
        star_graph[0][i]['t'] = '1'
    # assert cop_win(star_graph)

    # A dynamic star
    dynamic_star_graph = nx.complete_graph(11)
    for i in range(10):
        for j in range(i + 1, 11):
            dynamic_star_graph[i][j]['t'] = '00000000000000000000000000000'
    dynamic_star_graph[0][1]['t'] = '10111011101110111011101110111'  # x_0x_11
    dynamic_star_graph[0][2]['t'] = '10011001100110011001100110011'  # x_0x_21
    dynamic_star_graph[0][3]['t'] = '10001000100010001000100010001'  # x_0x_31
    dynamic_star_graph[0][4]['t'] = '10000000000000000000000000000'  # x_0x_41
    dynamic_star_graph[2][5]['t'] = '11111111111111111111111111111'  # x_21x_22
    dynamic_star_graph[3][6]['t'] = '11111111111111111111111111111'  # x_31x_32
    dynamic_star_graph[6][8]['t'] = '11111111111111111111111111111'  # x_32x_33
    dynamic_star_graph[4][7]['t'] = '11111111111111111111111111111'  # x_41x_42
    dynamic_star_graph[7][9]['t'] = '11111111111111111111111111111'  # x_42x_43
    dynamic_star_graph[9][10]['t'] = '11111111111111111111111111111' # x_43x_44
    assert cop_win(dynamic_star_graph)

    # Another dynamic star
    dynamic_star_graph = nx.complete_graph(11)
    for i in range(10):
        for j in range(i + 1, 11):
            dynamic_star_graph[i][j]['t'] = '00000000'
    dynamic_star_graph[0][1]['t'] = '10111111'  # x_0x_11
    dynamic_star_graph[0][2]['t'] = '11001111'  # x_0x_21
    dynamic_star_graph[0][3]['t'] = '11100011'  # x_0x_31
    dynamic_star_graph[0][4]['t'] = '11110000'  # x_0x_41
    dynamic_star_graph[2][5]['t'] = '11111111'  # x_21x_22
    dynamic_star_graph[3][6]['t'] = '11111111'  # x_31x_32
    dynamic_star_graph[6][8]['t'] = '11111111'  # x_32x_33
    dynamic_star_graph[4][7]['t'] = '11111111'  # x_41x_42
    dynamic_star_graph[7][9]['t'] = '11111111'  # x_42x_43
    dynamic_star_graph[9][10]['t'] = '11111111' # x_43x_44
    assert cop_win(dynamic_star_graph)

    # A dynamic cycle graph of length 4
    dynamic_cycle_graph = nx.cycle_graph(4)
    dynamic_cycle_graph[0][1]['t'] = '01'
    dynamic_cycle_graph[1][2]['t'] = '11'
    dynamic_cycle_graph[2][3]['t'] = '11'
    dynamic_cycle_graph[3][0]['t'] = '11'
    dynamic_cycle_graph.add_edge(0, 2, t='00')
    dynamic_cycle_graph.add_edge(1, 3, t='00')
    assert cop_win(dynamic_cycle_graph)

    # Two cycle graphs of lenght 5 alterning 
    dynamic_robber_win_graph = nx.complete_graph(5)
    dynamic_robber_win_graph[0][1]['t'] = '10'
    dynamic_robber_win_graph[0][2]['t'] = '01'
    dynamic_robber_win_graph[0][3]['t'] = '01'
    dynamic_robber_win_graph[0][4]['t'] = '10'
    dynamic_robber_win_graph[1][2]['t'] = '10'
    dynamic_robber_win_graph[1][3]['t'] = '01'
    dynamic_robber_win_graph[1][4]['t'] = '01'
    dynamic_robber_win_graph[2][3]['t'] = '10'
    dynamic_robber_win_graph[2][4]['t'] = '01'
    dynamic_robber_win_graph[3][4]['t'] = '10'
    assert not cop_win(dynamic_robber_win_graph)

    # Test of the verbose
    # cop_win(dynamic_star_graph, True)
    # cop_win(dynamic_cycle_graph, True)
