# On dynamic cop-win graphs

## Introduction
The Cops and Robbers game was first introduced independently by Quilliot, and by Nowakowski and Winkler. This game is played by a cop and a robber who place themselves on vertices of a graph and take turns moving along an edge. Cop plays first. He wins if at any moment he occupies the same vertex as the robber. If the robber can avoid capture forever, he wins.

Dynamic graphs can be defined as a 3-tuple: a set of vertices, a set of edges and a presence function that map each edge to a binary sequence. The i-th bit of a specific sequence means if the associated edge is present at the i-th time step in the graph.

The purpose of this repository is to test the algorithm designed in the article "On dynamic cop-win graphs".

## Getting started
The only file in this project is the [script](https://gitlab.com/gabrielfortinleblanc/on-dynamic-cop-win-graphs/-/blob/main/dynamic_cop_win_graph.py). The algorithm uses the [Networkx](https://networkx.org/) library to manage the graphs.

### Setup the environment
We consider that the user uses Python 3.3+ and use `venv` package to manage 
his virtual environments. For more information about virtual environment,
please visit the [venv documentation](https://docs.python.org/3/library/venv.html).
```shell
python3 -m venv path/to/environment  # Create the environment
source path/to/environment/bin/activate  # Activate the environment
pip install -r requirements.txt
```
Finally, to execute the tests, you have to execute the script. If nothing is printed from this script, then the tests passed, otherwise an assertion error will be printed.
```shell
python dynamic_cop_win_graph.py
```
